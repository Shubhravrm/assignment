const service = require("../services/userService");


function addUser(req, res) {
  service
    .addUser(req.body)
    .then((result) => {
      res.status(201).send(result);
    })
    .catch((err) => {
      res.status(400).send({ message: err.message });
    });
}

function getUserById(req, res) {
  service
    .getUserById(req.params.user_id)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(400).send({ message: err.message });
    });
}

function updateUserById(req, res) {
  service
    .updateUserById(req.params.user_id, req.body)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(400).send({ message: err.message });
    });
}

function deleteUserById(req, res) {
  service
    .deleteUserById(req.params.user_id)
    .then(() => {
      res.status(200).send({ message: "Deleted Successfully" });
    })
    .catch((err) => {
      res.status(400).send({ message: err.message });
    });
}

module.exports = { addUser, getUserById, updateUserById, deleteUserById };
