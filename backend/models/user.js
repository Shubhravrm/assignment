const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    age: { type: Number, required: true },
    sex: { type: String },
    school: { type: String },
    university: { type: String },
    maritalStatus: { type: String }
  },
  {
    timestamps: true
  }
);

const userModel = mongoose.model("users", UserSchema);

module.exports = userModel;