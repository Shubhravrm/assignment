const express = require("express");
const controller = require("../controllers/userController");
const router = express.Router();

router.route("/user").post((req, res) => {
  controller.addUser(req, res);
});

router.route("/user/:user_id").get((req, res) => {
  controller.getUserById(req, res);
});

router.route("/user/:user_id").patch((req, res) => {
  controller.updateUserById(req, res);
});

router.route("/user/:user_id").delete((req, res) => {
  controller.deleteUserById(req, res);
})

module.exports = router;
