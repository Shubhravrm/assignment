const userModel = require("../models/user");

async function addUser(data) {
  try {
    const userData = new userModel(data);
    const res = await userData.save();
    return res;
  } catch (error) {
    throw error;
  }
}

async function getUserById(userId) {
  try {
    const user = await userModel.findById({ _id: userId }, { __v: 0 });
    return user;
  } catch (error) {
    throw error;
  }
}

async function updateUserById(userId, data) {
  try {
    const updatedUser = await userModel.findOneAndUpdate({ _id: userId }, { $set: data, upsert: true }, { new: true });
    return updatedUser;
  } catch (error) {
    throw error;
  }
}

async function deleteUserById(userId) {
  try {
    await userModel.findByIdAndDelete({ _id: userId });
  } catch (error) {
    throw error;
  }
}

module.exports = { addUser, getUserById, updateUserById, deleteUserById };
